import { callApi } from '../helpers/apiHelper';
import { IFighter } from '../types';

export async function getFighters(): Promise<IFighter[]> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult = await callApi<IFighter[]>(endpoint, 'GET');

    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<IFighter> {
  // endpoint - `details/fighter/${id}.json`;
  try {
    const endpoint: string = `details/fighter/${id}.json`;
    const apiResult = await callApi<IFighter>(endpoint, 'GET')

    return apiResult;
  } catch (error) {
    throw error;
  }
}

