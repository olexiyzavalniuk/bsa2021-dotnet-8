import { createElement } from './helpers/domHelper';
import { IFighter, FighterSelector } from './types';

export type FighterClickHandler = (event: Event, fighter: IFighter) => void;

export function createFighter(fighter: IFighter, handleClick: FighterClickHandler, selectFighter: FighterSelector): HTMLDivElement {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighter' });

  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name: string): HTMLSpanElement {
  const nameElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string): HTMLImageElement {
  const imgElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter-image',
    attributes: {
      src: source
    }
  });

  return imgElement;
}

function createCheckbox(): HTMLLabelElement {
  const label = createElement<HTMLLabelElement>({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement<HTMLSpanElement>({ tagName: 'span', className: 'checkmark' });
  const checkboxElement = createElement<HTMLInputElement>({
    tagName: 'input',
    attributes: {
      type: 'checkbox'
    }
  });

  label.append(checkboxElement, span);
  return label;
}
