import { IFighter } from "./types";

export function fight(...fighters: IFighter[]): IFighter {
  const [firstFighter, secondFighter] = fighters;
  // return winner

  let health1 = firstFighter.health;
  let health2 = secondFighter.health;

  while (health1 > 0 && health2 > 0) {
    health2 -= getDamage(firstFighter, secondFighter);
    health1 -= getDamage(secondFighter, firstFighter);
  }

  if (health1 > health2) {
    return firstFighter;
  }
  return secondFighter;
}

export function getDamage(attacker: IFighter, enemy: IFighter): number {
  // damage = hit - block
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(enemy);

  if (damage > 0) {
    return damage;
  }

  return 0;
}

export function getHitPower(fighter: IFighter): number {
  // return hit power
  const criticalHitChance = 1 + Math.random();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: IFighter): number {
  // return block power
  const dodgeChance = 1 + Math.random();
  return fighter.defense * dodgeChance;
}

// DODGE CHANCE -> DOGE CHANCE
// ─────────▄──────────────▄
// ────────▌▒█───────────▄▀▒▌
// ────────▌▒▒▀▄───────▄▀▒▒▒▐
// ───────▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
// ─────▄▄▀▒▒▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
// ───▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀██▀▒▌
// ──▐▒▒▒▄▄▄▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄▒▒▌
// ──▌▒▒▐▄█▀▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐
// ─▐▒▒▒▒▒▒▒▒▒▒▒▌██▀▒▒▒▒▒▒▒▒▀▄▌
// ─▌▒▀▄██▄▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▒▌
// ─▌▀▐▄█▄█▌▄▒▀▒▒▒▒▒▒░░░░░░▒▒▒▐    THANKS FOR CHANCE!!!
// ▐▒▀▐▀▐▀▒▒▄▄▒▄▒▒▒▒▒░░░░░░▒▒▒▒▌
// ▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒░░░░░░▒▒▒▐
// ─▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒▒▒░░░░▒▒▒▒▌
// ─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐
// ──▀▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▌
// ────▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀
// ───▐▀▒▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀
// ──▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▀
