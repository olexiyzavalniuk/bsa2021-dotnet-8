import { createElement } from '../helpers/domHelper';
import { IFighter } from '../types';
import { showModal } from "./modal";

export function showWinnerModal(fighter: IFighter) {
    // show winner name and image
  const bodyElement = createWinnerDetails(fighter);
  const title = 'Winner';
  showModal({title, bodyElement});
}

function createWinnerDetails(fighter: IFighter): HTMLDivElement {
  const name = fighter.name;

  const fighterDetails = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'fighter-name' });

  const imageElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter-image',
    attributes: {
      src: fighter.source
    }
  });
  nameElement.innerText = `Name: ${name}\n`;

  fighterDetails.append(nameElement, imageElement);

  return fighterDetails;
}
