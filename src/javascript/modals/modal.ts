import { createElement } from '../helpers/domHelper';
import { IModalAttrs } from '../types';

export function showModal({ title, bodyElement }: IModalAttrs): void {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement);

  root.append(modal);
}

function getModalContainer(): HTMLElement | null {
  return document.getElementById('root');
}

function createModal(title: string, bodyElement: HTMLDivElement): HTMLDivElement {
  const layer = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string): HTMLDivElement {
  const headerElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement<HTMLSpanElement>({ tagName: 'span' });
  const closeButton = createElement<HTMLDivElement>({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);

  return headerElement;
}

function hideModal(event: Event) {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
