import { createElement } from '../helpers/domHelper';
import { IFighter } from '../types';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter: IFighter) {
  const title: string = 'Info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: IFighter): HTMLDivElement {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'fighter-name' });

  // show fighter name, attack, defense, health, image
  const healthElement = createElement<HTMLSpanElement>({tagName: 'span', className: 'fighter-health'});
  const attackElement = createElement<HTMLSpanElement>({tagName: 'span', className: 'fighter-attack'});
  const defenseElement = createElement<HTMLSpanElement>({tagName: 'span', className: 'fighter-defense'});

  healthElement.innerText = `Health: ${health}\n`;
  nameElement.innerText = `Name: ${name}\n`;
  attackElement.innerText = `Attack: ${attack}\n`;
  defenseElement.innerText = `Defense: ${defense}\n`;

  const imageElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter-image',
    attributes: {
      src: source
    }
  });

  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}
