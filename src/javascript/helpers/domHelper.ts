import { IElementAttrs } from "../types";

export function createElement<TElement extends HTMLElement>({ tagName, className, attributes = {} }: IElementAttrs): TElement {
  const element = <TElement>document.createElement(tagName);

  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}
