import { IFighter } from '../types';
import { fightersDetails, fighters } from './mockData';

type HTTPMethod = "GET" | "POST" | "PUT" | "DELETE";

interface IFetchOptions {
  method: HTTPMethod;
}

type ResolveCallback<TResponse> = (value?: TResponse | PromiseLike<TResponse>) => void

type RejectCallback = (reason?: any) => void;

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi<TResponse>(endpoint: string, method: HTTPMethod): Promise<TResponse> {
  const url = API_URL + endpoint;
  const options: IFetchOptions = { method };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response: Response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error: Error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<IFighter | IFighter[]> {
  const response: IFighter | IFighter[] = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve: ResolveCallback<IFighter | IFighter[]>, reject: RejectCallback) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighter {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
