import { IFighter } from "../types"

export const fighters: IFighter[] = [
  {
    "_id": "1",
    "name": "Uther the Lightbringer",
    "source": "https://64.media.tumblr.com/b82c2decb573ea787bec842fb6ed2658/tumblr_o5v8r5bJsu1qkpz2go1_r1_400.gifv"
  }, {
    "_id": "2",
    "name": "Illidan Stormrage",
    "source": "https://64.media.tumblr.com/06be4be00ddb1f20e627b8f8b41caeb9/tumblr_o68u0vgd0p1qkpz2go1_r1_400.gifv"
  }, {
    "_id": "3",
    "name": "Jaina Proudmoore",
    "source": "https://64.media.tumblr.com/633c397a11ea40a9cfcf7037f46327d5/tumblr_o60wukcP2g1qkpz2go1_500.gifv"
  }, {
    "_id": "4",
    "name": "Kael'thas Sunstrider",
    "source": "https://64.media.tumblr.com/1d6a637156bed9959dff880632de7860/tumblr_o7y00elB7P1qkpz2go1_500.gifv"
  }, {
    "_id": "5",
    "name": "Sylvanas Windrunner",
    "source": "https://64.media.tumblr.com/af9c2a796605a377f95baf6b40b115c2/tumblr_o5tx2xwU2J1qkpz2go1_400.gifv"
  }, {
    "_id": "6",
    "name": "Thrall",
    "source": "https://64.media.tumblr.com/46182258fbed75ae31d46e35ee1551c7/tumblr_odf538avNs1qkpz2go1_1280.gifv"
  }
]

export const fightersDetails: IFighter[] = [
  {
    "_id": "1",
    "name": "Uther the Lightbringer",
    "health": 60,
    "attack": 3.3,
    "defense": 4.7,
    "source": "https://64.media.tumblr.com/b82c2decb573ea787bec842fb6ed2658/tumblr_o5v8r5bJsu1qkpz2go1_r1_400.gifv"
  }, {
    "_id": "2",
    "name": "Illidan Stormrage",
    "health": 50,
    "attack": 4.9,
    "defense": 3.1,
    "source": "https://64.media.tumblr.com/06be4be00ddb1f20e627b8f8b41caeb9/tumblr_o68u0vgd0p1qkpz2go1_r1_400.gifv"
  }, {
    "_id": "3",
    "name": "Jaina Proudmoore",
    "health": 50,
    "attack": 4.5,
    "defense": 3.5,
    "source": "https://64.media.tumblr.com/633c397a11ea40a9cfcf7037f46327d5/tumblr_o60wukcP2g1qkpz2go1_500.gifv"
  }, {
    "_id": "4",
    "name": "Kael'thas Sunstrider",
    "health": 45,
    "attack": 5.5,
    "defense": 2.5,
    "source": "https://64.media.tumblr.com/1d6a637156bed9959dff880632de7860/tumblr_o7y00elB7P1qkpz2go1_500.gifv"
  }, {
    "_id": "5",
    "name": "Sylvanas Windrunner",
    "health": 40,
    "attack": 7,
    "defense": 1.6,
    "source": "https://64.media.tumblr.com/af9c2a796605a377f95baf6b40b115c2/tumblr_o5tx2xwU2J1qkpz2go1_400.gifv"
  }, {
    "_id": "6",
    "name": "Thrall",
    "health": 60,
    "attack": 3.5,
    "defense": 4.5,
    "source": "https://64.media.tumblr.com/46182258fbed75ae31d46e35ee1551c7/tumblr_odf538avNs1qkpz2go1_1280.gifv"
  }
]
