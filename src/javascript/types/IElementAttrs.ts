export default interface IElementAttrs {
    tagName: string;
    className?: string;
    attributes?: Record<string, any>
  };
  