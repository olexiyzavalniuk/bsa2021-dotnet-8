import IFighter from "./IFighter";

export type FighterSelector = (event: Event, fighter: IFighter) => Promise<void>;
