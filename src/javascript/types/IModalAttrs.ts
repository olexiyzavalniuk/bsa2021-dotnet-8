export default interface IModalAttrs {
    title: string;
    bodyElement: HTMLDivElement;
  }
  