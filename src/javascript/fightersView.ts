import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService'
import { IFighter, FighterSelector } from './types';

export function createFighters(fighters: IFighter[]): HTMLDivElement {
  const selectFighterForBattle: FighterSelector = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<string, IFighter>();

async function showFighterDetails(event: Event, fighter: IFighter): Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighter> {
  // get fighter from fightersDetailsCache or use getFighterDetails function

  if (fightersDetailsCache.has(fighterId)) {
    return fightersDetailsCache.get(fighterId);
  }

  const fighter = await getFighterDetails(fighterId);
  return fighter;
}

function createFightersSelector(): FighterSelector {
  const selectedFighters = new Map<string, IFighter>();

  return async function selectFighterForBattle(event: Event, fighter: IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    const eventTarget = <HTMLInputElement>event.target;

    if (eventTarget.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner = fight(...selectedFighters.values());
      showWinnerModal(winner);
    }
  }
}
